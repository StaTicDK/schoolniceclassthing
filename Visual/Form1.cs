﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using LiveCharts;
using LiveCharts.Wpf;
using Newtonsoft.Json;
using SchoolLibrary;

namespace Visual
{
    public partial class Form1 : Form
    {
        /// <summary>
        ///     list of loaded schools
        /// </summary>
        public List<School> schools = new List<School>();

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     load file to schoolList
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "json files (*.json)|*.json";
                ofd.Multiselect = true;
                ofd.CheckFileExists = true;

                if (ofd.ShowDialog() != DialogResult.OK) return;
                foreach (string filename in ofd.FileNames)
                    schools.Add(JsonConvert.DeserializeObject<School>(File.ReadAllText(filename)));
                UpdatePie();
            }
        }

        /// <summary>
        ///     loads all students in the students list
        /// </summary>
        private void UpdatePie()
        {
            pcHeightDistr.Series.Clear();
            List<Student> students = new List<Student>();
            foreach (School school in schools) students.AddRange(school.Students);
            Dictionary<ushort, uint> studentsPerHeight = new Dictionary<ushort, uint>();

            foreach (Student student in students)
                if (studentsPerHeight.ContainsKey(student.Height))
                    studentsPerHeight[student.Height]++;
                else
                    studentsPerHeight[student.Height] = 1;
            //sort the studentperheight by height
            studentsPerHeight = studentsPerHeight.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
            chart1.Series["StudentsPerHeight"].Points.Clear();
            foreach (KeyValuePair<ushort, uint> heightCount in studentsPerHeight)
            {
                pcHeightDistr.Series.Add(new PieSeries
                {
                    Title = heightCount.Key.ToString(),
                    Values = new ChartValues<int> {(int) heightCount.Value},
                    DataLabels = false,
                    Margin = new Thickness(0, 0, 0, 0),
                    StrokeThickness = 0
                    /*LabelPoint = labelPoint*/
                });
                chart1.Series["StudentsPerHeight"].Points.AddXY(heightCount.Key, heightCount.Value);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        /// <summary>
        ///     clicking the clear button clears the data in the pie chart
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            schools.Clear();
            pcHeightDistr.Series.Clear();
            chart1.Series["StudentsPerHeight"].Points.Clear();
        }

        private void chart1_Click(object sender, EventArgs e)
        {
        }
    }
}