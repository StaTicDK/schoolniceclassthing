﻿using System;
using System.Collections.Generic;
using SchoolLibrary;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
namespace AutoGenerator
{
    internal static class RoomGenerator
    {
        private static readonly Random R = new Random();

        private static string GetRandomName()
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string result = "";

            for (byte i = 0; i < R.Next(0, 10); i++) result += chars[R.Next(0, chars.Length)];
            return result;
        }

        private static uint GetRandomNumber()
        {
            return (uint) R.Next(0, 100000);
        }

        public static List<Room> GenerateRooms(int amount)
        {
            List<Room> rooms = new List<Room>();
            for (int i = 0; i < amount; i++) rooms.Add(new Room(GetRandomName(), GetRandomNumber()));
            return rooms;
        }
    }
}