﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using SchoolLibrary;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
namespace AutoGenerator
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Random r = new Random();
            string dir = "";
            if (File.Exists("setup.ini"))
            {
                string[] lines = File.ReadAllLines("setup.ini");
                string[] temp = lines[0].Split('=');
                dir = temp[1];
            }
            else
            {
                Console.WriteLine("Please enter folder path: ");
                string dirtemp = Console.ReadLine();

                string temp = "";
                if (!(dirtemp is null))
                    foreach (char t in dirtemp)
                        if (t == '\\')
                        {
                            temp += "\\";
                            temp += "\\";
                        }
                        else
                        {
                            temp += t;
                        }

                using (StreamWriter writer = new StreamWriter("setup.ini"))
                {
                    writer.WriteLine("dir=" + temp);
                }

                dir = temp;
            }

            List<string> names = JsonConvert.DeserializeObject<List<By>>(File.ReadAllText("zipcodes.json"))
                .Select(by => by.City).ToList();
            string schoolname = names.ElementAt(new Random().Next(0, names.Count - 1));

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (Directory.Exists(dir + '\\' + "Schools") && Directory.GetFiles(dir + '\\' + "Schools") != null)
            {
                string[] files = Directory.GetFiles(dir + '\\' + "Schools");

                foreach (string file in files)
                {
                    School school = JsonConvert.DeserializeObject<School>(File.ReadAllText(file));
                    if (school != null) Console.WriteLine(school.ToString());
                }
            }
            else
            {
                Console.WriteLine("Please enter Amount to number generate");
                short amount = Convert.ToInt16(Console.ReadLine());

                List<Student> people = StudentGen.StudentGenerator(amount * 60, schoolname);
                Console.WriteLine("TEST1");
                List<Staff> staff = StaffGen.StaffGenerator(amount * 30, schoolname);
                Console.WriteLine("TEST2");
                List<Room> room = RoomGenerator.GenerateRooms(amount * 10);
                Console.WriteLine("TEST3");
                List<Course> course = CourseAutoGenerator.GenerateCourses(ref staff, amount * 5);
                Course.Save(course, dir);
                Console.WriteLine("TEST4");
                List<Klasse> klasse = KlasseGenerator.GenerateClasses(amount * 10, ref people, ref course);
                Console.WriteLine("TEST5");
                IEnumerable<Lesson> lessons = LessonGen.LessonGenerator(amount * 20, ref room, ref course, ref klasse);
                Console.WriteLine("TEST6");
                SchoolGenerator.GenerateSchools(amount, ref people, ref staff, ref course, ref klasse, ref room,
                    ref schoolname, ref dir);
                Console.WriteLine("TEST7");
            }

            Console.ReadLine();
        }
    }
}