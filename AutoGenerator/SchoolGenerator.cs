﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using SchoolLibrary;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable SuspiciousTypeConversion.Global
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable SuggestVarOrType_Elsewhere
namespace AutoGenerator
{
    internal static class SchoolGenerator
    {
        private static readonly Random R = new Random();

        /// <summary>
        ///     Generates a random list of students
        /// </summary>
        /// <param name="students"></param>
        /// <returns>The generated random list of students</returns>
        private static List<Student> GetSublistStudents(ref List<Student> students)
        {
            List<Student> student = new List<Student>();

            for (byte j = 0; j < R.Next(1, 100); j++)
                student.Add(students.ElementAt(R.Next(0, students.Count)));

            return student;
        }

        private static List<Staff> GetSublistStaff(ref List<Staff> staff)
        {
            List<Staff> staffList = new List<Staff>();

            for (byte j = 0; j < R.Next(1, 100); j++)
                staffList.Add(staff.ElementAt(R.Next(0, staff.Count)));

            return staffList;
        }

        private static List<Course> GetSublistCourse(ref List<Course> courses)
        {
            List<Course> courseList = new List<Course>();

            for (byte j = 0; j < R.Next(1, 100); j++)
                courseList.Add(courses.ElementAt(R.Next(0, courses.Count)));

            return courseList;
        }

        private static List<Klasse> GetSublistClasses(ref List<Klasse> classes)
        {
            List<Klasse> classList = new List<Klasse>();

            for (byte j = 0; j < R.Next(1, 100); j++)
                classList.Add(classes.ElementAt(R.Next(0, classes.Count)));

            return classList;
        }

        private static List<Room> GetSublistRooms(ref List<Room> rooms)
        {
            List<Room> roomList = new List<Room>();

            for (byte j = 0; j < R.Next(1, 100); j++)
                roomList.Add(rooms.ElementAt(R.Next(0, rooms.Count)));

            return roomList;
        }

        public static void GenerateSchools(int amount, ref List<Student> students, ref List<Staff> staff,
            ref List<Course> courses, ref List<Klasse> classes, ref List<Room> rooms, ref string schoolname,
            ref string dir)
        {
            List<By> byer = JsonConvert.DeserializeObject<List<By>>(File.ReadAllText("zipcodes.json"));
            List<string> names = byer?.Select(by => by.City).ToList();

            // For loop for creating new schools
            for (int i = 0; i < amount; i++)
            {
                var studentList = GetSublistStudents(ref students);
                var staffList = GetSublistStaff(ref staff);
                var courseList = GetSublistCourse(ref courses);
                var classList = GetSublistClasses(ref classes);
                var roomList = GetSublistRooms(ref rooms);

                School school = new School(schoolname, studentList, staffList, courseList, classList, roomList);
                School.Save(school, dir);
            }
        }
    }
}