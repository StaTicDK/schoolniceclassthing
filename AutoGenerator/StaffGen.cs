﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using SchoolLibrary;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
namespace AutoGenerator
{
    internal static class StaffGen
    {
        public static List<Staff> StaffGenerator(int count, string schoolname)
        {
            List<Staff> staff = new List<Staff>();
            List<Fornavn> fornavne = JsonConvert.DeserializeObject<List<Fornavn>>(File.ReadAllText("fornavne.json"));
            List<string> efternavne = JsonConvert.DeserializeObject<List<string>>(File.ReadAllText("efternavne.json"));
            List<string> vejnavne = JsonConvert.DeserializeObject<List<string>>(File.ReadAllText("vejnavne.json"));
            List<By> byer = JsonConvert.DeserializeObject<List<By>>(File.ReadAllText("zipCodes.json"));
            char[] vejnummerchar = {' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
            Random random = new Random();
            for (int i = 0; i < count; i++)
            {
                if (efternavne == null) continue;
                string efternavn = efternavne[random.Next(0, efternavne.Count - 1)];

                if (fornavne == null) continue;
                string name = fornavne[random.Next(0, fornavne.Count - 1)].Navn + " " + efternavn;

                DateTime birthday =
                    new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(
                        random.Next(315529200, 1577833200));

                if (byer == null) continue;
                string zipcode = byer[random.Next(0, byer.Count - 1)].ZipCode;
                if (vejnavne == null) continue;
                string vejnavn = vejnavne[random.Next(0, vejnavne.Count - 1)];
                string bynavn = byer[random.Next(0, byer.Count - 1)].City;
                string vejnummer = random.Next(1, 300).ToString() +
                                   vejnummerchar[random.Next(1, vejnummerchar.Length - 1)];

                Adresse address = new Adresse(zipcode, vejnavn, bynavn, vejnummer);

                string number = random.Next(0, 99999999).ToString();
                if (number.Length < 8)
                    for (int b = number.Length; b < 8; b++)
                        number = "0" + number;

                string phoneNumber = "+45" + number;
                string email = efternavn + "@" + schoolname.Replace(" ", ".") + ".com";
                ushort height = (ushort) random.Next(80, 210);
                ushort accessLevel = (ushort) random.Next(0, 2);

                Staff staffmember = new Staff(name, birthday, address, phoneNumber, email, height,
                    random.Next(0, 1).ToString(), accessLevel);

                staff.Add(staffmember);
            }

            return staff;
        }
    }
}