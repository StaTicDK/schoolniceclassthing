﻿using System;
using System.Collections.Generic;
using System.Linq;
using SchoolLibrary;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable once SuggestVarOrType_BuiltInTypes
namespace AutoGenerator
{
    internal static class KlasseGenerator
    {
        private static readonly Random R = new Random();

        private static List<Student> GetRandomStudents(ref List<Student> students)
        {
            List<Student> studentList = new List<Student>();
            for (byte i = 0; i < R.Next(0, 100); i++)
                studentList.Add(students.ElementAt(R.Next(0, students.Count - 1)));
            return studentList;
        }

        private static List<Course> GetRandomCourses(ref List<Course> courses)
        {
            List<Course> courseList = new List<Course>();
            for (byte i = 0; i < R.Next(0, 100); i++)
                courseList.Add(courses.ElementAt(R.Next(0, courses.Count)));
            return courseList;
        }

        private static string GetName()
        {
            const string numbers = "0123456789";
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            string result = "";
            result += numbers[R.Next(0, numbers.Length)];
            result += ".";
            result += chars[R.Next(0, chars.Length)];

            return result;
        }

        public static List<Klasse> GenerateClasses(int amount, ref List<Student> students, ref List<Course> courses)
        {
            List<Klasse> classes = new List<Klasse>();
            for (int i = 0; i < amount; i++)
            {
                string name = GetName();
                List<Student> studentList = GetRandomStudents(ref students);
                List<Course> courseList = GetRandomCourses(ref courses);

                classes.Add(new Klasse(name, studentList, courseList));
            }

            return classes;
        }
    }
}