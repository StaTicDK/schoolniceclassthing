﻿using System;
using System.Collections.Generic;
using System.Linq;
using SchoolLibrary;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable once SuggestVarOrType_BuiltInTypes
namespace AutoGenerator
{
    internal static class CourseAutoGenerator
    {
        private static readonly string[] Subjects = {"Matematik", "Dansk", "Engelsk", "Programmering"};

        private static readonly string[] Names = {"Course1", "Course2", "Course3", "Course4"};


        public static List<Course> GenerateCourses(ref List<Staff> staffs, int count = 300)
        {
            List<Course> list = new List<Course>();
            Random rand = new Random();

            //Opret courses
            for (int i = 0; i < count; i++)
            {
                var newCourse = new Course
                {
                    Name = Names[rand.Next(Names.Length - 1)],
                    Subject = Subjects[rand.Next(Subjects.Length - 1)],
                    Description = "peep ee",
                    Teachers = staffs.ElementAt(rand.Next(0, staffs.Count - 1)),
                    AmountOfLessons = rand.Next(255)
                };
                list.Add(newCourse);
            }

            return list;
        }
    }
}