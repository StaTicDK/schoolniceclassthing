﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using SchoolLibrary;

namespace SchoolSearch
{
    internal static class Program
    {
        private static void Main()
        {
            Console.WriteLine("Specify path to folder (for schools)");
            string schoolsPath = Console.ReadLine();
            if (schoolsPath == null) return;
            List<School> schools = Directory.GetFiles(schoolsPath)
                .Select(school => JsonConvert.DeserializeObject<School>(File.ReadAllText(school))).ToList();

            if (schools.Count == 0)
            {
                Console.WriteLine("There is no schools in the currect directory, try autogenerating some");
                Console.WriteLine("Closing application...");
                return;
            }

            while (true)
            {
                Console.WriteLine("Write search query or q for quitting");
                string sadlife = Console.ReadLine();

                if (sadlife != null && sadlife.Equals("q"))
                    break;
                List<Student> result = schools.SelectMany(t =>
                        (from bruh in t.Students select bruh).Where(p => sadlife != null && p.Name.Contains(sadlife)))
                    .ToList();
                Console.WriteLine(JsonConvert.SerializeObject(result));
            }
        }
    }
}