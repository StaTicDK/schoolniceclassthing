﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using SchoolLibrary;

namespace SearchApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Specify path to folder (for schools)");
            string schoolsPath = Console.ReadLine();
            List<School> schools = new List<School>();
            foreach (string school in Directory.GetFiles(schoolsPath))
            {
                schools.Add(JsonConvert.DeserializeObject<School>(File.ReadAllText(school)));
            }
            while (true)
            {
                List<Student> result = new List<Student>();
                Console.WriteLine("Write search query");
                string sadlife = Console.ReadLine();
                for (int i = 0; i < schools.Count; i++)
                {
                    IEnumerable<Student> rows = (from bruh in schools[i].Students select bruh).Where(p => p.Name.Contains(sadlife));
                    foreach (Student row in rows)
                    {
                        result.Add(row);
                    }
                }
                Console.WriteLine(JsonConvert.SerializeObject(result));
                
            }
        }
        
    }
}