﻿using System;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable once MemberCanBePrivate.Global
namespace SchoolLibrary
{
    public abstract class Grade
    {
        protected Grade(Course course, string mark, DateTime date)
        {
            Course = course;
            Mark = mark;
            Date = date;
        }

        private Course Course { get; }


        public string Mark { get; set; }

        private DateTime Date { get; }
    }
}