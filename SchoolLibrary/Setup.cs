﻿//Setup.cs fil

using System.IO;
using System.Linq;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable global MemberCanBePrivate.Global
namespace SchoolLibrary
{
    public class Setup
    {
        private static Setup _setup;

        private Setup()
        {
            if (!File.Exists("setup.ini")) return;
            string[] lines = File.ReadAllLines("setup.ini");

            foreach (string line in lines)
            {
                if (line.Length <= 0 || line[0] == '#') continue;
                string[] option = line.Split('=');
                if (option.Length == 2)
                {
                    option[0] = option[0].Trim('"').Trim();
                    option[1] = option[1].Trim('"').Trim();
                    switch (option[0])
                    {
                        case "dataPath":
                            DataPath = option[1].Last() == '\\' && option[1].Length > 0 ? option[1] : option[1] + "\\";
                            break;
                    }
                }
            }
        }

        public string DataPath { get; }

        public static Setup GetSetup()
        {
            return _setup ?? (_setup = new Setup());
        }
    }
}