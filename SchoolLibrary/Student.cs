﻿using System;
using System.Collections.Generic;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable global MemberCanBePrivate.Global
namespace SchoolLibrary
{
    public class Student : Person
    {
        public Student(string name,
            DateTime birthday,
            Adresse address,
            string phoneNumber,
            string email,
            ushort height,
            ushort accessLevel,
            List<Klasse> classes)
            : base(name,
                birthday,
                address,
                phoneNumber,
                email,
                height,
                accessLevel)
        {
            Classes = classes;
        }

        private List<Klasse> Classes { get; }

        private List<Grade> Grades { get; set; } = new List<Grade>();
    }
}