﻿

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
namespace SchoolLibrary
{
    public class By
    {
        public By(string city, string zipCode)
        {
            City = city;
            ZipCode = zipCode;
        }

        public string ZipCode { get; }

        public string City { get; }
    }
}