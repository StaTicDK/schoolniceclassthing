﻿

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
namespace SchoolLibrary
{
    public class Fornavn
    {
        public Fornavn(string navn)
        {
            Navn = navn;
        }

        public string Navn { get; set; }
    }
}