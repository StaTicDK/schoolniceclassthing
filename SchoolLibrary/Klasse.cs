﻿using System.Collections.Generic;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable once MemberCanBePrivate.Global
namespace SchoolLibrary
{
    public class Klasse
    {
        public Klasse(string name, List<Student> students, List<Course> courses)
        {
            Name = name;
            Students = students;
            Courses = courses;
        }

        public string Name { get; set; }

        private List<Student> Students { get; }
        private List<Course> Courses { get; }
    }
}