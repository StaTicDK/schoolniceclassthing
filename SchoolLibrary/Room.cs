﻿// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable global MemberCanBePrivate.Global

namespace SchoolLibrary
{
    public class Room
    {
        public Room(string name, uint number)
        {
            Name = name;
            Number = number;
        }

        public string Name { get; set; }

        public uint Number { get; set; }
    }
}