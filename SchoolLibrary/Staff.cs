﻿using System;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable global MemberCanBePrivate.Global
namespace SchoolLibrary
{
    public class Staff : Person
    {
        public Staff(string name,
            DateTime birthday,
            Adresse address,
            string phoneNumber,
            string email,
            ushort height,
            string position,
            ushort accessLevel)
            : base(name,
                birthday,
                address,
                phoneNumber,
                email,
                height,
                accessLevel)
        {
            Position = position;
        }

        private string Position { get; }
    }
}