﻿using System;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable once MemberCanBePrivate.Global
namespace SchoolLibrary
{
    public class Lesson
    {
        public Course Course { get; set; }

        public DateTime StartTime { get; set; }

        public Room Room { get; set; }

        public DateTime EndTime { get; set; }

        public Klasse Klasse { get; set; }
    }
}