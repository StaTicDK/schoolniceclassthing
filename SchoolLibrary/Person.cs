﻿using System;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable once MemberCanBePrivate.Global
namespace SchoolLibrary
{
    public class Person
    {
        protected Person(string name,
            DateTime birthday,
            Adresse address,
            string phoneNumber,
            string email,
            ushort height,
            ushort accessLevel)
        {
            Name = name;
            Birthday = birthday;
            Address = address;
            PhoneNumber = phoneNumber;
            Email = email;
            Height = height;
            AccessLevel = accessLevel;
        }

        public string Name { get; set; }

        private DateTime Birthday { get; }

        public Adresse Address { get; set; }

        private string PhoneNumber { get; }

        public string Email { get; set; }

        public ushort Height { get; set; }

        private ushort AccessLevel { get; }
    }
}