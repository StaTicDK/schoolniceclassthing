﻿

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
namespace SchoolLibrary
{
    public class Adresse
    {
        public Adresse(string zipcode, string vejnavn, string bynavn, string vejnummer)
        {
            ZipCode = zipcode;
            VejNavn = vejnavn;
            ByNavn = bynavn;
            VejNummer = vejnummer;
        }

        public string ZipCode { get; }

        private string ByNavn { get; }

        private string VejNavn { get; }

        private string VejNummer { get; }

        public override string ToString()
        {
            return VejNavn + " " + VejNummer + ", " + ZipCode + " " + ByNavn;
        }
    }
}