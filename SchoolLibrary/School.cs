﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable global MemberCanBePrivate.Global

namespace SchoolLibrary
{
    public class School
    {
        private const string CourseDir = "Schools";

        private static Dictionary<string, School> _schools;

        public School(string name, List<Student> students, List<Staff> staffs, List<Course> courses,
            List<Klasse> classes, List<Room> rooms)
        {
            Name = name;
            Students = students;
            Staffs = staffs;
            Courses = courses;
            Classes = classes;
            Rooms = rooms;
        }

        public string Name { get; set; }
        public List<Student> Students { get; set; }
        public List<Staff> Staffs { get; set; }
        public List<Course> Courses { get; set; }
        public List<Klasse> Classes { get; set; }
        public List<Room> Rooms { get; set; }

        private void CreateClass(string name, List<Student> students, List<Course> courses)
        {
            Klasse klasse = new Klasse(name, students, courses);
            Classes.Add(klasse);
        }

        private void CreateNewStudent(string name,
            DateTime birthday,
            Adresse address,
            string phoneNumber,
            string email,
            ushort height,
            ushort accessLevel,
            List<Klasse> classes)
        {
            Student student = new Student(name, birthday, address,
                phoneNumber, email, height,
                accessLevel, classes);
            Students.Add(student);
        }

        private void CreateNewStaff(string name,
            DateTime birthday,
            Adresse address,
            string phoneNumber,
            string email,
            ushort height,
            string position,
            ushort accessLevel)
        {
            Staff newStaff = new Staff(name, birthday, address,
                phoneNumber, email, height,
                position, accessLevel);
            Staffs.Add(newStaff);
        }

        private string GetHash()
        {
            byte[] hash = new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(Name + Students.First() +
                Staffs.First() + Courses.First() + Classes.First() + Rooms.First()));

            StringBuilder sOutput = new StringBuilder(hash.Length);
            foreach (byte t in hash) sOutput.Append(t.ToString("X2"));

            return sOutput.ToString();
        }

        private static ref Dictionary<string, School> GetList()
        {
            if (_schools != null) return ref _schools;
            _schools = new Dictionary<string, School>();

            string dirPath = Setup.GetSetup().DataPath + CourseDir;
            if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath);

            foreach (string file in Directory.GetFiles(dirPath))
            {
                School add = JsonConvert.DeserializeObject<School>(File.ReadAllText(file));
                if (add != null) _schools.Add(add.GetHash(), add);
            }

            return ref _schools;
        }

        public static School Find(string key)
        {
            if (_schools == null) GetList();

            if (_schools != null && _schools.TryGetValue(key, out School add)) return add;

            return null;
        }

        public static void Save(School school, string path)
        {
            string dirPath = path + '\\' + CourseDir;
            if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath);

            using (StreamWriter writer = new StreamWriter(dirPath + "\\" + school.GetHash() + ".json"))
            {
                writer.WriteLine(JsonConvert.SerializeObject(school));
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}